export type ComponentTemplates = Array<ComponentSchema>;

export interface ComponentSchema {
  id: ComponentCreator
}

export type ComponentCreator = () => {
  remove: RemoveCallback,
  add: AddCallback,
  edit: EditCallback
}
export type RemoveCallback = () => void;
export type AddCallback = () => void;
export type EditCallback = (element: Element) => void;

export const setup = (componentTemplates: ComponentTemplates, document: Document, config: SetupConfig = getDefaultSetupConfig()) => {
  document.documentElement.children


}

export type SetupConfig = {
  domParser: DOMParser;
}

const getDefaultSetupConfig = (): SetupConfig => {
  return {
    domParser: new DOMParser()
  }
}

const appendEditRecursively = (childNodes: NodeListOf<ChildNode>) => {
  childNodes.forEach((nodeEl) => {
    appendEditRecursively(nodeEl.childNodes);
  });
  }
}

const createDomUtils = (domParser: DOMParser) => {
  const parseHtmlToDocument = (html: string): Document => {
    return domParser.parseFromString(html, 'text/html');
  }

  const parseDocumentToHtml = (htmlDom: Document): string => {
    return htmlDom.documentElement.outerHTML;
  }
}

