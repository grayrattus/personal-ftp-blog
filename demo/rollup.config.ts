export default {
  input: 'src/index.ts',
  output: {
    file: 'bundle/index.js',
    format: 'cjs'
  }
};

