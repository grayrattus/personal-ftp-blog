# Description

This is my PoC of application that will `allow to host your website on some FTP server and treat it as CMS when credentials are provided`.

Basic idea is like this.

You create `index.html` that will contain web components in HTML.
Next you only have to add set of web components that under the hood use `personal-ftp-blog` library to

- add possibility to edit HTML
- add possibility to add new components to HTML
- upload updated HTML to FTP server when user is authenticated
- create FTP versioning for changes for easy backup/restore functionality
- every text on the page will be clickable and editable, user should only click on text and edit it whenever there is a need for it

So it will basically allow user to edit their HTML with web components and upload shadow dom HTML to FTP. Next if someone visits this FTP server hosting `index.html` they will see the page with updates. 

It's the simples form of CMS that should allow anyone to host their own websites with templates.
