function createRemove() {
  return {
    remove: undefined
  }
}

function createEditComponentFunctionality(callback) {
  const save = document.createElement('button');
  save.value = 'save';
  save.addEventListener('click', () => {
    callback();
  });
  return save;
}

function createEditableElement(element, removeCallback) {
      console.log(removeCallback);
      const form = document.createElement('form');
      const input = document.createElement('input');
    
      form.appendChild(input);
      form.style.display = 'none';
      element.style.display = 'inline-block';
      input.style.width = element.clientWidth + 'px';

      element.appendChild(form);

      input.value = element.innerText;

      element.addEventListener('click', () => {
        console.log(element);
        form.style.display = 'inline-block';
        form.style.position = 'absolute';
        form.style.pointerEvents = 'all';
        form.style.top = '0px'
        form.style.left = '0px';
        input.focus();
        input.setSelectionRange(0, input.value.length)
      });

      form.addEventListener('submit', e => {
        updateDisplay();
        e.preventDefault();
      });

      input.addEventListener('blur', updateDisplay);

      removeCallback.remove = () => {
        form.remove();
      }

      function updateDisplay() {
        form.style.pointerEvents = 'none';
        form.style.display = 'none';
        element.textContent = input.value;
        createEditableElement(element, removeCallback);
      }
}

customElements.define('component-with-data',
  class extends HTMLElement {
    constructor() {
      super();


      console.log(this.children);
      this.personName = this.children[0].innerText;
      this.personAge = this.children[1].innerText;
      this.personOccupation = this.children[2].innerText;

      const removeCallbacks = [];
      [...this.children].forEach(el => {
        const remove = createRemove();
        removeCallbacks.push(remove);
        createEditableElement(el, remove);
      });


      const children = [...this.children];

      const shadowRoot = this.attachShadow({mode: 'open'});
      const removeAllFormCallbacks = () => {
        removeCallbacks.forEach(r => r.remove());
      }
      const saveButton = createEditComponentFunctionality(removeAllFormCallbacks);

      shadowRoot.appendChild(children[0]);
      shadowRoot.appendChild(children[1]);
      shadowRoot.appendChild(children[2]);
      shadowRoot.appendChild(saveButton);

    }
  }
);
